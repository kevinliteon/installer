#!/usr/bin/env bash

# Update
sudo pacman -Syyu

# Paquets officiels
sudo pacman -S yaourt yaourt-gui zsh vim tmux docker docker-compose skype htop gimp

# Paquets AUR
sudo yaourt phpstorm hipchat sublime-text-dev google-chrome spotify google-cloud-sdk powerline-fonts

# Oh my ZSH
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
